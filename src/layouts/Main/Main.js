import React, { useEffect } from "react";

//libs
import classnames from "classnames";
import parse from 'parse-duration';

// styles
import useStyles from "./styles";

// components
import { Header, Sidebar } from "../../components";

// context
import { useLayoutState } from "../../context/LayoutContext";
import { useUserContext } from "../../context/UsersContext";

//API services
import { authService } from '../../services';

const Main = (props) => {
  const { setIsAuthenticated, user, setUser } = useUserContext();
  const classes = useStyles();
  const { children } = props;

  // global
  var layoutState = useLayoutState();

  const refreshingToken = () => {
    console.log("periodic: time to refresh token");
    authService.getCurrentUser()
      .then(
        (res) => {
          setIsAuthenticated(true)
        }
      )
      .catch(
        err => {
          setIsAuthenticated(false);
          console.log("Session Expired!");
          setUser({});
        });
  };

  useEffect(() => {
    const intervalId = setInterval(refreshingToken, parse(user.expiresIn) - 60000);   // exp time - 1 min
    return () => clearInterval(intervalId)
  }, []);

  return (
    <div className={classes.root}>
      <>
        <Header history={props.history} />
        <Sidebar />
        <div
          className={classnames(classes.content, {
            [classes.contentShift]: layoutState.isSidebarOpened,
          })}
        >
          <div className={classes.fakeToolbar} />
          {children}
        </div>
      </>
    </div>
  );
}

export default Main;
