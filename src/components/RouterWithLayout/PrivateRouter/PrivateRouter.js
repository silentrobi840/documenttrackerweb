import React from 'react';

//libs
import { Route, Redirect, useLocation } from 'react-router-dom';
import PropTypes from 'prop-types';

//context
import { useUserContext } from "../../../context/UsersContext";

const PrivateRouter = (props) => {
    const { layout: Layout, component: Component, ...rest } = props;
    const { isAuthenticated } = useUserContext();
    const { pathname, search } = useLocation();
    return (
        <Route
            {...rest}
            render={matchProps => (
                isAuthenticated
                    ?
                    <Layout>
                        <Component {...matchProps} />
                    </Layout>
                    : <Redirect to={
                        `/login?redirect=${pathname}${search}`
                    } />
            )}
        />
    );
};

PrivateRouter.propTypes = {
    component: PropTypes.any.isRequired,
    layout: PropTypes.any.isRequired,
    path: PropTypes.string
};

export default PrivateRouter;
