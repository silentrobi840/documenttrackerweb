import React from 'react';

//libs
import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

//context
import { useUserContext } from "../../../context/UsersContext";

function querystring(name, url = window.location.href) {
    name = name.replace(/[[]]/g, "\\$&");

    const regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)", "i");
    const results = regex.exec(url);

    if (!results) {
        return null;
    }
    if (!results[2]) {
        return "";
    }

    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
const PublicRouter = (props) => {
    const { component: Component, ...rest } = props;
    const { isAuthenticated } = useUserContext();
    const redirect = querystring("redirect");
    return (
        <Route
            {...rest}
            render={matchProps => (
                !isAuthenticated ?
                React.createElement(Component, matchProps)
                    : <Redirect to={redirect === "" || redirect === null ? "/records" : redirect} />
            )}
        />
    );
};

PublicRouter.propTypes = {
    component: PropTypes.any.isRequired,
    layout: PropTypes.any.isRequired,
    path: PropTypes.string
};

export default PublicRouter;
