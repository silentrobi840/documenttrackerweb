import React, { useState, useEffect } from 'react';

//styles
import { makeStyles } from '@material-ui/core/styles';

//libs
import clsx from "clsx";

//UI libs
import {
    Icon,
    Grid,
    CssBaseline,
    Dialog,
    Slide
} from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    container: {
        backgroundColor: 'rgba(0,0,0,0.9)' /* Black w/ opacity */
    },
    close: {
        position: 'absolute',
        top: '8px',
        right: '8px',
        color: '#f1f1f1',
        fontSize: '40px',
        fontWeight: 'bold',
        transition: '0.3s',
        '&:hover, &:focus': {
            color: '#bbb',
            textDecoration: 'none',
            cursor: 'pointer'
        }
    },
    modelContainer: {
        position: 'relative',
        margin: 'auto',
        paddingTop: '100px',/* Location of the box */
        maxWidth: '400px',
        width: '80%',
        display: 'block',
        animationName: `$zoom`,
        animationDuration: '0.6s',
        [theme.breakpoints.up(780)]: {
            width: '100%'
        }
    },
    prev: {
        cursor: 'pointer',
        position: 'absolute',
        top: '50%',
        width: 'auto',
        marginTop: '-22px',
        left: 0,
        padding: '16px',
        color: 'white',
        fontWeight: 'bold',
        fontSize: '18px',
        transition: '0.6s ease',
        borderRadius: '0 3px 3px 0',
        userSelect: 'none',
        '&:hover': {
            backgroundColor: 'rgba(0,0,0,0.8)'
        }
    },
    next: {
        cursor: 'pointer',
        position: 'absolute',
        top: '50%',
        width: 'auto',
        marginTop: '-22px',
        padding: '16px',
        color: 'white',
        fontWeight: 'bold',
        fontSize: '18px',
        transition: '0.6s ease',
        userSelect: 'none',
        right: 0,
        borderRadius: '3px 0 0 3px',
        '&:hover': {
            backgroundColor: 'rgba(0,0,0,0.8)'
        }
    },
    '@keyframes zoom': {
        from: { transform: 'scale(0)' },
        to: { transform: 'scale(1)' }
    },
    wrapContentHeading: {
        display: 'flex',
        flexDirection: 'row-reverse',
        justifyContent: 'space-between',
        alignItems: 'start'
    },
    title: {
        marginLeft: theme.spacing(2),
        flex: 1,
    },
    dot: {
        cursor: 'pointer',
        height: '15px',
        width: '15px',
        margin: '0 2px',
        backgroundColor: '#bbb',
        borderRadius: '50%',
        display: 'inline-block',
        transition: 'background-color 0.6s ease',
        '&:hover': {
            backgroundColor: '#717171',
        },

    },
    activeLink: {
        backgroundColor: '#717171',
    },
    /* Fading animation */
    fade: {
        '-webkit-animation-name': `$fade`,
        '-webkit-animation-duration': '1.5s',
        animationName: `$fade`,
        animationDuration: '1.5s'
    },

    '@-webkit-keyframes fade':
    {
        from: { opacity: .4 },
        to: { opacity: 1 }
    },

    '@keyframes fade': {
        from: { opacity: .4 },
        to: { opacity: 1 }
    }
}));

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const ModelImage = (props) => {
    const classes = useStyles();
    const { open, onClose, modelImages, clickedImageIndex } = props;
    const [slideIndex, setSlideIndex] = useState(clickedImageIndex);
    const [currentImage, setCurrentImage] = useState(modelImages[0]);
    const size = modelImages.length;
    const swipeUp = () => {

        if (slideIndex === (size - 1)) {
            setSlideIndex(0);
        }
        else {
            let newIndex = slideIndex + 1;
            setSlideIndex(newIndex);
        }
    };
    const swipeDown = () => {
        if (slideIndex === 0) {
            setSlideIndex(size - 1);
        }
        else {
            let newIndex = slideIndex - 1;
            setSlideIndex(newIndex);
        }
    };

    const changeImage = (index) => {
        setSlideIndex(index);
    };

    useEffect(() => {
        setCurrentImage(modelImages[slideIndex]);
    }, [slideIndex]);

    return (
        <div>
            <Dialog PaperProps={{
                style: {
                    backgroundColor: 'rgb(0,0,0)', /* Fallback color */
                    backgroundColor: 'rgba(0,0,0,0.9)', /* Black w/ opacity */
                    boxShadow: 'none'
                },
            }}
                fullScreen={true}
                fullWidth={true}
                onClose={onClose}
                open={open}
            >

                <div className={classes.wrapContentHeading}>

                    <Icon
                        className={classes.close}
                        onClick={onClose}
                    >
                        &times;
                    </Icon>
                </div>
                <CssBaseline />

                <Grid
                    container
                    direction="column"
                    justify="center"
                    alignItems="center"
                    spacing={0}
                >
                    <Grid item xs={12}>
                        <div className={classes.fade}>
                            <img
                                src={currentImage}
                                alt={currentImage}
                                className={classes.modelContainer} />
                        </div>
                        <a className={classes.prev} onClick={swipeDown}>&#10094;</a>
                        <a className={classes.next} onClick={swipeUp}>&#10095;</a>
                        <br />
                        <div style={{ textAlign: 'center' }}>
                            {[...Array(size)]
                                .map((e, i) =>
                                    <span key={i}
                                        className={clsx({
                                            [classes.dot]: true,
                                            [classes.activeLink]: slideIndex === i
                                        })}
                                        underline='none'
                                        onClick={() => setSlideIndex(i)}
                                        key={i}>
                                    </span>
                                )}
                        </div>
                    </Grid>
                </Grid>
            </Dialog >
        </div >
    );
}

export default ModelImage;