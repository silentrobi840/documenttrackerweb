import React from 'react';

//UI libs
import {
    Menu,
    MenuItem,
    ListItemText,
    ListItemIcon
} from '@material-ui/core';
import SettingsIcon from '@material-ui/icons/Settings';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';

const UserOptions = (props) => {
    const { handleClose,
        anchorEl,
        handleUserOptionSelect } = props;
    return (
        <>
            <Menu
                anchorEl={anchorEl}
                onClose={handleClose}
                open={Boolean(anchorEl)}
                getContentAnchorEl={null}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
            >
                <MenuItem onClick={() => handleUserOptionSelect("profile-setting")}>
                    <ListItemIcon>
                        <SettingsIcon fontSize="small" />
                    </ListItemIcon>
                    <ListItemText primary="Profile Setting" />
                </MenuItem>
                <MenuItem onClick={() => handleUserOptionSelect("sign-out")}>
                    <ListItemIcon>
                        <ExitToAppIcon fontSize="small" />
                    </ListItemIcon>
                    <ListItemText primary="Sign Out" />
                </MenuItem>
            </Menu>
        </>
    );
}

export default UserOptions; 