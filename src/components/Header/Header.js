import React, { useState } from "react";

//libs
import { Link as RouterLink, useHistory } from 'react-router-dom';
import classNames from "classnames";

import {
  AppBar,
  Toolbar,
  IconButton,
  Tooltip,
  Typography
} from "@material-ui/core";
import {
  Menu as MenuIcon,
  ArrowBack as ArrowBackIcon,
  AccountCircle as AccountCircleIcon
} from "@material-ui/icons";

// styles
import useStyles from "./styles";

// components
import { UserOptions } from './components';

// context
import {
  useLayoutState,
  useLayoutDispatch,
  toggleSidebar,
} from "../../context/LayoutContext";
import { useUserContext } from "../../context/UsersContext";

//API services
import { authService } from '../../services';

export default function Header(props) {
  var classes = useStyles();

  // global
  var layoutState = useLayoutState();
  var layoutDispatch = useLayoutDispatch();
  const { setIsAuthenticated, user, setUser } = useUserContext();
  // local
  const history = useHistory();
  var [mailMenu, setMailMenu] = useState(null);
  var [isMailsUnread, setIsMailsUnread] = useState(true);
  var [notificationsMenu, setNotificationsMenu] = useState(null);
  var [isNotificationsUnread, setIsNotificationsUnread] = useState(true);
  var [profileMenu, setProfileMenu] = useState(null);
  var [isSearchOpen, setSearchOpen] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);

  const handleUserOptionClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleUserOptionSelect = (id) => {
    setAnchorEl(null);
    switch (id) {
      case "profile-setting":
        history.push("/profile-setting");
        break;
      case "sign-out":
        authService.logout();
        setIsAuthenticated(false);
        history.push('/login');
        break;
    }
  };

  return (
    <AppBar position="fixed" className={classes.appBar}>
      <Toolbar className={classes.toolbar}>
        <IconButton
          color="inherit"
          onClick={() => toggleSidebar(layoutDispatch)}
          className={classNames(
            classes.headerMenuButton,
            classes.headerMenuButtonCollapse,
          )}
        >
          {layoutState.isSidebarOpened ? (
            <ArrowBackIcon
              classes={{
                root: classNames(
                  classes.headerIcon,
                  classes.headerIconCollapse,
                ),
              }}
            />
          ) : (
              <MenuIcon
                classes={{
                  root: classNames(
                    classes.headerIcon,
                    classes.headerIconCollapse,
                  ),
                }}
              />
            )}
        </IconButton>
        <Typography variant="h6" weight="medium" className={classes.logotype}>
          Document Record Management
        </Typography>
        <div className={classes.grow} />
        <Tooltip title="User Account">
          <IconButton
            className={classes.userOption}
            color="inherit"
            onClick={handleUserOptionClick}
          >
            <AccountCircleIcon />
          </IconButton>
        </Tooltip>
        <UserOptions anchorEl={anchorEl} handleClose={handleClose} handleUserOptionSelect={handleUserOptionSelect} />
      </Toolbar>
    </AppBar>
  );
}
