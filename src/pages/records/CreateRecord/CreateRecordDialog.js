import React, { useState, useEffect } from 'react';

//libs
import PropTypes from 'prop-types';
import validate from 'validate.js';
import { useHistory } from 'react-router-dom';
import "font-awesome/css/font-awesome.min.css";
import { v1 as uuidv1 } from 'uuid';

//UI libs
import {
    Close as CloseIcon,
    Cancel as CancelIcon,
    Add as AddIcon
} from '@material-ui/icons';
import {
    Button,
    TextField,
    Dialog,
    DialogTitle,
    Typography,
    Grid,
    Divider,
    IconButton,
    CircularProgress
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

//API services
import { recordService, cloudinaryService } from '../../../services';

const IMAGE_SLOTS = 3;

const useStyles = makeStyles((theme) => ({
    content: {
        margin: theme.spacing(2)
    },
    contentFooter: {
        marginTop: theme.spacing(2)
    },
    input: {
        display: 'none',
    },
    circularProgress: {
        marginLeft: 0,
        marginRight: theme.spacing(1),
    },
    button: {
        paddingLeft: theme.spacing(1),
        margin: theme.spacing(1)
    },
    wrapContentHeading: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    childContent: {
        marginTop: theme.spacing(2)
    },
    closeIcon: {
        marginRight: theme.spacing(-1)
    },
    imageContainer: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
        margin: theme.spacing(2)
    },
    image: {
        position: 'relative',
        '& img': {
            display: 'block',
            width: '150px',
            height: 'auto',
            borderRadius: '8px',
            border: '1px solid #ddd',
            padding: '5px',
            transition: '0.3s',
            '&:hover': {
                opacity: 0.7
            }
        },
        marginTop: theme.spacing(2)
    },
    addIcon: {
        position: 'relative',
        width: '100px',
        borderRadius: '8px',
        border: '1px solid #ddd',
        padding: '5px',
        height: 'auto',
        display: 'block',
        marginTop: theme.spacing(2),
        transition: '0.3s',
        '&:hover, &:focus': {
            color: '#bbb',
            textDecoration: 'none',
            cursor: 'pointer'
        }
    },
    wrapImageIcon: {
        position: 'absolute',
        top: -10,
        right: -10,
        cursor: 'pointer'
    }
}));

const schema = {
    from: {
        presence: { allowEmpty: false, message: 'is required' },
        length: {
            maximum: 255
        }
    },
    subject: {
        presence: { allowEmpty: false, message: 'is required' },
    },
    description: {
        presence: { allowEmpty: false, message: 'is required' },
    }
};

export default function CreateRecordDialog(props) {
    const [loading, setLoading] = useState(false);
    const [imageIds, setImageIds] = useState([]);
    const history = useHistory();
    const classes = useStyles();
    const { onClose, open, refresh } = props;
    const [images, setImages] = useState([null, null, null]);
    const [formState, setFormState] = useState({
        isValid: false,
        values: {
        },
        touched: {},
        errors: {}
    });
    const resetImage = (event, i) => {
        event.preventDefault();
        let currentImages = images.slice();
        currentImages[i] = null;
        console.log(currentImages);
        setImages(currentImages);
    };
    const handleChange = event => {
        event.persist();
        setFormState(formState => ({
            ...formState,
            values: {
                ...formState.values,
                [event.target.name]: event.target.value
            },
            touched: {
                ...formState.touched,
                [event.target.name]: true
            }
        }));
    };

    const hasError = field =>
        formState.touched[field] && formState.errors[field] ? true : false;

    const closeDialog = (action) => {
        onClose(action);
        refresh(true);
    };

    const handleCreate = () => {
        setLoading(true);
        const uploadSignRequestpromises = [];
        const uploadImagePromises = [];
        const imageIds = [];
        const uploadImages = [];
        for (let i = 0; i < images.length; i++) {
            if (images[i] !== null) {
                uploadImages.push(images[i]);
            }
        }

        if (uploadImages.length === 0) {
            recordService.createRecord({
                ...formState.values,
                image: ''
            })
                .catch(err => {
                    if (err.response.body.errors === "Invalid token") console.log("Invalid token");
                });
            setLoading(false);
            closeDialog('create');
            return;
        }

        for (let i = 0; i < uploadImages.length; i++) {

            const uniqueImageId = uuidv1();
            uploadSignRequestpromises.push(
                recordService.getUploadApiSignature({
                    public_id: uniqueImageId,
                    folder: 'mail_tracker_images',
                    access_mode: 'authenticated',
                    invalidate: 'true'
                })
            );
        }
        Promise.all(uploadSignRequestpromises)
            .then(
                (result) => {
                    result.map(
                        (res, i) => {
                            uploadImagePromises.push(
                                cloudinaryService.uploadImage({ ...res.body }, uploadImages[i])
                            )
                        });
                    return Promise.all(uploadImagePromises);
                })
            .then(
                (result) => {
                    result.map(
                        (res) => {
                            imageIds.push(res.body.secure_url);
                        }
                    );

                    const imageString = imageIds.join(',');
                    recordService.createRecord({
                        ...formState.values,
                        images: imageString
                    }).catch(err => {
                        if (err.response.body.errors === "Invalid token") console.log("invalid token")
                    });

                    setLoading(false);
                    closeDialog('create');
                })
            .catch((err) => {
                if (err.body &&
                    err.response.body.errors &&
                    err.response.body.errors === "Invalid token"
                ) console.log("invalid token");
            });
    };

    const handleImageUpload = (e, i) => {
        const image = e.target.files[0];
        let currentImages = images.slice();
        currentImages[i] = image;
        console.log(currentImages);
        setImages(currentImages);

    };

    useEffect(() => {
        const errors = validate(formState.values, schema);
        console.log(images);
        setFormState(formState => ({
            ...formState,
            // overwirte values that we get from --> ...formState spred operation
            isValid: errors ? false : true,
            errors: errors || {}
        }));
        const keys = Object.keys(formState.values);
        if (keys.length !== 0) {
            for (const key of keys) {
                if (formState.values[key].length > 0) {
                    //setHasUnsaveData(true);
                    return;
                }

            }
            //setHasUnsaveData(false);
        }
    }, [formState.values]);

    return (
        <Dialog
            maxWidth="sm"
            fullWidth={true}
            onClose={onClose}
            open={open}
            disableBackdropClick
            disableEscapeKeyDown
        >
            <DialogTitle id="simple-dialog-title" >
                <div className={classes.wrapContentHeading}>
                    <Typography align="left" variant="body1" style={{ display: 'inline-block' }}>
                        <b> Create Main Record </b>
                    </Typography>
                    <IconButton
                        className={classes.closeIcon}
                        disabled={loading}
                        onClick={onClose}>
                        <CloseIcon />
                    </IconButton>
                </div>
            </DialogTitle>
            <Divider />
            <div className={classes.content}>
                <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                >
                    <Grid item xs={12}>
                        <Grid item>
                            <TextField
                                error={hasError('from')}
                                helperText={
                                    hasError('from') ? formState.errors.from[0] : null
                                }
                                label="From"
                                name="from"
                                type="text"
                                fullWidth
                                onChange={handleChange}
                                value={formState.values.from || ''}
                                variant="outlined"
                            />
                            <TextField
                                error={hasError('subject')}
                                helperText={
                                    hasError('subject') ? formState.errors.subject[0] : null
                                }
                                label="Subject"
                                name="subject"
                                type="text"
                                fullWidth
                                onChange={handleChange}
                                value={formState.values.subject || ''}
                                variant="outlined"
                                className={classes.childContent}
                            />
                            <TextField
                                error={hasError('description')}
                                helperText={
                                    hasError('description') ? formState.errors.description[0] : null
                                }
                                label="Description"
                                name="description"
                                type="text"
                                fullWidth
                                onChange={handleChange}
                                value={formState.values.description || ''}
                                variant="outlined"
                                className={classes.childContent}
                            />
                            <div className={classes.imageContainer}>
                                {[...images]
                                    .map((e, i) =>
                                        images[i] === null ?
                                            <div key={i}>
                                                <input
                                                    accept="image/*"
                                                    className={classes.input}
                                                    id={`upload_${i}`}
                                                    multiple={false}
                                                    type="file"
                                                    onChange={(e) => handleImageUpload(e, i)}
                                                />
                                                <label htmlFor={`upload_${i}`}>
                                                    <AddIcon className={classes.addIcon} />
                                                </label>
                                            </div> :
                                            <div key={i}>
                                                <div className={classes.image} >
                                                    <img src={URL.createObjectURL(images[i])} />
                                                    <CancelIcon
                                                        color="error"
                                                        onClick={
                                                            (e) => resetImage(e, i)
                                                        }
                                                        className={classes.wrapImageIcon} />
                                                </div>
                                            </div>
                                    )
                                }
                            </div>
                        </Grid>
                        <Grid
                            container
                            item
                            direction="row-reverse"
                            justify="flex-start"
                            alignItems="flex-start"
                        >
                            <Grid item>
                                <Button
                                    variant="contained"
                                    color="primary"
                                    disabled={!formState.isValid || loading}
                                    onClick={handleCreate}
                                    className={classes.button}
                                >
                                    {loading &&
                                        <>
                                            <CircularProgress className={classes.circularProgress} size={20} thickness={5} />
                                            Saving...
                                        </>}
                                    {!loading && <span> Create </span>}
                                </Button>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </div>
        </Dialog>
    );
};

CreateRecordDialog.propTypes = {
    onClose: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired
};