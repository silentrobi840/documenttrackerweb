import React, { useEffect, useState } from "react";

//libs
import MUIDataTable from "mui-datatables";

//UI libs
import {
  Grid,
  Avatar,
  IconButton,
  ThemeProvider,
  Snackbar,
  Slide
} from "@material-ui/core";
import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles
} from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import { green } from '@material-ui/core/colors';
import MuiAlert from '@material-ui/lab/Alert';

// components
import EditRecordDialog from './EditRecord/EditRecordDialog';
import CreateRecordDialog from './CreateRecord/CreateRecordDialog';
import { ModelImage } from "../../components";

//API services
import { recordService } from '../../services';

const headerStyle = {
  backgroundColor: '#e0e0e0',
  fontWeight: 'bold'
}
const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex'
  },
  addIcon: {
    paddingBottom: theme.spacing(2),
    position: 'relative'
  },
  avater: {
    transition: '0.2s',
    '&:hover': {
      transform: 'scale(1.5,1.5)',
    },
    '&:not(:first-child)': {
      marginLeft: '-1rem',
    },
    '&:hover ~ &': {
      transform: 'translateX(1rem)'
    },
    cursor: 'pointer'
  }
}));

const getMuiTheme = () => createMuiTheme({
  overrides: {
    MUIDataTableHeadCell: {
      root: {
        '&:nth-child(1)': { //from
          width: 100
        },
        '&:nth-child(2)': { //subject
          width: 100
        },
        '&:nth-child(3)': { //description
          width: 400
        },
        '&:nth-child(5)': { //description
          width: 60
        },
      }
    },
  }
})

const theme = createMuiTheme({
  palette: {
    primary: green,
  },
});

function TransitionRight(props) {
  return <Slide {...props} direction="right" />;
}

export default function Records() {
  const [refreshTable, setRefreshTable] = useState(false);
  const [openCreateSnackbar, setOpenCreateSnackbar] = useState(false);
  const [openDeleteSnackbar, setOpenDeleteSnackbar] = useState(false);
  const [openUpdateSnackbar, setOpenUpdateSnackbar] = useState(false);
  const [editRowData, setEditRowData] = useState({});
  const [showImage, setShowImage] = useState(false);
  const [modelImages, setModelImages] = useState([]);
  const classes = useStyles();
  const [records, setRecords] = useState([]);
  const [openUpdateDialog, setOpenUpdateDialog] = useState(false);
  const [clickedImageIndex, setClickedImageIndex] = useState();
  const [openCreateDialog, setOpenCreateDialog] = useState(false);

  const handleEditClick = (rowData) => {
    setEditRowData(rowData);
    setOpenUpdateDialog(true);
  };

  const handleAddClick = () => {
    setOpenCreateDialog(true);
  };

  const handleCreateSnakebarClose = () => {
    setOpenCreateSnackbar(false);
  };

  const handleDeleteSnakebarClose = () => {
    setOpenDeleteSnackbar(false);
  };

  const handleCreateDialog = (action) => {
    setOpenCreateDialog(false);
    if (action === 'create') setOpenCreateSnackbar(true);
  };

  const handleUpdateSnakebarClose = () => {
    setOpenUpdateSnackbar(false);
  };

  const handleUpdateDialog = (action) => {
    setOpenUpdateDialog(false);
    if (action === 'update') setOpenUpdateSnackbar(true);
    else if (action === 'delete') setOpenDeleteSnackbar(true);
  };

  const handleImageClick = (images, clickedImageIndex) => {
    setShowImage(true);
    setModelImages(images.split(','));
    setClickedImageIndex(clickedImageIndex);
  };

  const handleCloseModelImage = () => {
    setShowImage(false);

  };

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  const columns = [
    {
      name: "mid",
      options: {
        display: false,
      }
    },
    {
      name: "from",
      label: "From",
      options: {
        filter: true,
        sort: true,
        setCellHeaderProps: (value) => ({ style: headerStyle }),
      }
    },
    {
      name: "subject",
      label: "Subject",
      options: {
        filter: true,
        sort: true,
        setCellHeaderProps: (value) => ({ style: headerStyle }),
      }
    },
    {
      name: "description",
      label: "Description",
      options: {
        searchable: false,
        sort: false,
        setCellHeaderProps: (value) => ({ style: headerStyle }),
      }
    },
    {
      name: "images",
      label: "Images",
      options: {
        searchable: false,
        sort: false,
        setCellHeaderProps: (value) => ({ style: headerStyle }),
        customBodyRender: (value, tableMeta, updateValue) => (
          <div className={classes.root}>
            {value && value.split(',').map(
              (image, i) => (
                <Avatar
                  onClick={() => handleImageClick(value, i)}
                  className={classes.avater}
                  key={i}
                  alt={image}
                  src={image} />
              )
            )}
          </div>
        )
      }
    },
    {
      name: "Action",
      label: "Action",
      options: {
        searchable: false,
        sort: false,
        empty: true,
        setCellHeaderProps: (value) => ({ style: headerStyle }),
        customBodyRender: (value, tableMeta, updateValue) => {
          return (
            <div >
              <ThemeProvider theme={theme}>
                <IconButton color='primary' component="span" onClick={() => handleEditClick(tableMeta.rowData)}>
                  <i className="fa fa-pencil" aria-hidden="true"></i>
                </IconButton>
              </ThemeProvider>
            </div>
          );
        }
      }
    }
  ];

  useEffect(() => {
    recordService.getRecords()
      .then(res => {
        console.log(res);
        setRecords(res.body);
      }).catch((e) => {
        console.log(e.response.body.errors);
        if (e.response.body.errors === "Invalid token") {
          console.log(e.response.body.errors);
        }
      });
    return () => {
      setRefreshTable(false);
    }
  }, [records.length, refreshTable]);

  return (
    <>
      <MuiThemeProvider theme={getMuiTheme()}>
        <Grid
          container
          direction="row-reverse"
          justify="flex-start"
          alignItems="center"
        >
          <div className={classes.addIcon}>
            <Fab color="primary" aria-label="add" onClick={handleAddClick}>
              <AddIcon />
            </Fab>
          </div>
        </Grid>
        <Grid container spacing={4}>
          <Grid item xs={12}>
            <MUIDataTable
              title="Document Records"
              data={records}
              columns={columns}
              options={{
                download: false,
                print: false,
                viewColumns: false,
                filter: false,
                selectableRows: false,
                responsive: 'standard',
                searchPlaceholder: "Seach by 'Subject' or 'From' column",
              }}
            />
          </Grid>
        </Grid>
        {
          openCreateDialog &&
          <CreateRecordDialog
            open={openCreateDialog}
            onClose={handleCreateDialog}
            refresh={setRefreshTable}
          />
        }
        {
          openUpdateDialog &&
          <EditRecordDialog
            open={openUpdateDialog}
            rowData={editRowData}
            onClose={handleUpdateDialog}
            refresh={setRefreshTable}
          />
        }
        {showImage && <ModelImage
          modelImages={modelImages}
          clickedImageIndex={clickedImageIndex}
          open={showImage}
          onClose={handleCloseModelImage} />}
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right',
          }}
          open={openCreateSnackbar}
          onClose={handleCreateSnakebarClose}
          autoHideDuration={3000}
        >
          <Alert onClose={handleCreateSnakebarClose} severity="success">
            New document record has been created!
        </Alert>
        </Snackbar>
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right',
          }}
          open={openUpdateSnackbar}
          onClose={handleUpdateSnakebarClose}
          autoHideDuration={3000}
        >
          <Alert onClose={handleUpdateSnakebarClose} severity="success">
            Document record has been updated!
        </Alert>
        </Snackbar>
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right',
          }}
          open={openDeleteSnackbar}
          onClose={handleDeleteSnakebarClose}
          autoHideDuration={3000}
        >
          <Alert onClose={handleDeleteSnakebarClose} severity="error">
            Document record has been deleted!
        </Alert>
        </Snackbar>
      </MuiThemeProvider>

    </>
  );
}
