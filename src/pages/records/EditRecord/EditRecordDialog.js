import React, { useState, useEffect } from 'react';

//config
import config from '../../../config/cloudinary.config';

//libs
import PropTypes from 'prop-types';
import validate from 'validate.js';
import { useHistory } from 'react-router-dom';
import "font-awesome/css/font-awesome.min.css";
import { v1 as uuidv1 } from 'uuid';

//UI libs
import {
    Button,
    TextField,
    Dialog,
    DialogTitle,
    Typography,
    Grid,
    Divider,
    IconButton,
    CircularProgress
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import {
    Close as CloseIcon,
    Cancel as CancelIcon,
    Add as AddIcon
} from '@material-ui/icons';

//API services
import { recordService, cloudinaryService } from '../../../services';

//context
import { useUserContext } from "../../../context/UsersContext";

const IMAGE_SLOTS = 3;

const useStyles = makeStyles((theme) => ({
    content: {
        margin: theme.spacing(2)
    },
    contentFooter: {
        marginTop: theme.spacing(2)
    },
    input: {
        display: 'none',
    },
    button: {
        margin: theme.spacing(2, 0, 0, 2),
    },
    wrapContentHeading: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    childContent: {
        marginTop: theme.spacing(2)
    },
    closeIcon: {
        marginRight: theme.spacing(-1)
    },
    imageContainer: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
        margin: theme.spacing(2)
    },
    circularProgress: {
        marginLeft: 0,
        marginRight: theme.spacing(1),
    },
    image: {
        position: 'relative',
        '& img': {
            display: 'block',
            width: '150px',
            height: 'auto',
            borderRadius: '8px',
            border: '1px solid #ddd',
            padding: '5px',
            transition: '0.3s',
            '&:hover': {
                opacity: 0.7
            }
        },
        marginTop: theme.spacing(2)
    },
    addIcon: {
        position: 'relative',
        width: '100px',
        borderRadius: '8px',
        border: '1px solid #ddd',
        padding: '5px',
        height: 'auto',
        display: 'block',
        marginTop: theme.spacing(2),
        cursor: 'pointer'
    },
    wrapImageIcon: {
        position: 'absolute',
        top: -10,
        right: -10,
        cursor: 'pointer'
    }
}));

const schema = {
    from: {
        presence: { allowEmpty: false, message: 'is required' },
        length: {
            maximum: 255
        }
    },
    subject: {
        presence: { allowEmpty: false, message: 'is required' },
    },
    description: {
        presence: { allowEmpty: false, message: 'is required' },
    }
};

export default function EditRecordDialog(props) {
    const { setShouldRefreshToken } = useUserContext();
    const [loading, setLoading] = useState(false);
    const [deleting, setDeleting] = useState(false);
    const { cloudName, uploadPreset } = config;
    const history = useHistory();
    const classes = useStyles();
    const { onClose, open, rowData, refresh } = props;
    const [trackImages, setTrackImages] = useState([...Array(IMAGE_SLOTS).fill({
        remoteUrl: null,
        localUrl: null,
        isModified: false
    })]);
    const [formState, setFormState] = useState({
        isValid: false,
        values: {
            from: rowData[1],
            subject: rowData[2],
            description: rowData[3]
        },
        touched: {},
        errors: {}
    });
    const handleChange = event => {
        event.persist();
        setFormState(formState => ({
            ...formState,
            values: {
                ...formState.values,
                [event.target.name]: event.target.value
            },
            touched: {
                ...formState.touched,
                [event.target.name]: true
            }
        }));
    };

    const handleImageUpload = (e, i) => {
        e.preventDefault();
        console.log('upload ', i);
        const image = e.target.files[0];
        let currentTrackImages = trackImages.slice();
        currentTrackImages[i] = {
            ...trackImages[i],
            localUrl: image,
            isModified: true
        }
        setTrackImages(currentTrackImages);
        console.log(currentTrackImages);

    };

    const resetImage = (event, i) => {
        console.log('reset ', i);
        event.preventDefault();
        let currentTrackImages = trackImages.slice();
        currentTrackImages[i] = {
            ...trackImages[i],
            localUrl: null,
            isModified: true
        };
        console.log(currentTrackImages)
        setTrackImages(currentTrackImages);
    };

    const closeDialog = (action) => {
        onClose(action);
        refresh(true);
    };

    const pareseRemoteImageUrlToDelete = (remoteUrl) => {
        let tokens = remoteUrl.slice(
            remoteUrl.search('upload')
        ).split('/');
        let imageId = "";
        console.log(tokens);
        if (tokens.length > 4) {
            let pathTokens = tokens.splice(3);
            imageId = pathTokens.join('/').split('.')[0];
            console.log(imageId)
        }
        else {
            imageId = tokens[tokens.length - 1].split('.')[0];
        }
        console.log(imageId)
        return imageId;
    };

    const handleDelete = () => {
        setDeleting(true);
        console.log(trackImages);
        const deleteImagePromises = [];
        const deleteSignRequestPromises = [];

        for (let i = 0; i < trackImages.length; i++) {
            if (trackImages[i].remoteUrl != null) {
                const existingImageId = pareseRemoteImageUrlToDelete(trackImages[i].remoteUrl);
                console.log(existingImageId);
                deleteSignRequestPromises.push(
                    recordService.getUploadApiSignature({
                        public_id: existingImageId,
                        invalidate: 'true'
                    }))
            }
        }

        if (deleteSignRequestPromises.length === 0) {
            recordService.deleteRecord(rowData[0]).catch(err => {
                if (err.response.body.errors === "Invalid token") setShouldRefreshToken(true);
            });
            return;
        }

        Promise.all(deleteSignRequestPromises)
            .then(
                result => {
                    result.map(
                        res => {
                            console.log(res.body);
                            deleteImagePromises.push(cloudinaryService.destroyImage({ ...res.body }));
                        }
                    );
                    return Promise.all(deleteImagePromises);
                }
            ).then(
                result => {
                    result.map(
                        res => {
                            console.log(res);
                            if (res.body.result && res.body.result === "ok") {
                                console.log("image deleted successfully");
                            }
                        }
                    );
                    recordService.deleteRecord(rowData[0]).catch(err => {
                        if (err.response.body.errors === "Invalid token") setShouldRefreshToken(true);
                    });
                    setDeleting(false);
                    closeDialog("delete");
                }
            ).catch(err => {
                if (err.body &&
                    err.response.body.errors &&
                    err.response.body.errors === "Invalid token"
                ) setShouldRefreshToken(true);
            });
    };

    const handleUpdate = (event) => {
        setLoading(true);
        const promises = [];
        const imageIds = [];
        const modifySignRequestPromises = [];
        const modifyImagePromises = [];
        const modifyImageCases = [];
        const modifiableImages = [];
        for (let i = 0; i < trackImages.length; i++) {
            if (trackImages[i].isModified) {
                if (trackImages[i].localUrl !== null && trackImages[i].remoteUrl === null) {
                    modifyImageCases.push(1);
                    modifiableImages.push(trackImages[i].localUrl);
                    const uniqueImageId = uuidv1();
                    modifySignRequestPromises.push(
                        recordService.getUploadApiSignature(
                            {
                                public_id: uniqueImageId,
                                folder: 'mail_tracker_images',
                                access_mode: 'authenticated',
                                invalidate: 'true'
                            }
                        )
                    );
                }
                else if (trackImages[i].localUrl !== null && trackImages[i].remoteUrl !== null) {
                    modifyImageCases.push(2);
                    let tokens = trackImages[i].remoteUrl.split('/');
                    const existingImageId = tokens[tokens.length - 1].split('.')[0];
                    modifiableImages.push(trackImages[i].localUrl);
                    modifySignRequestPromises.push(
                        recordService.getUploadApiSignature({
                            public_id: existingImageId,
                            folder: 'mail_tracker_images',
                            access_mode: 'authenticated',
                            invalidate: 'true'
                        })
                    );
                }
                else if (trackImages[i].localUrl === null && trackImages[i].remoteUrl !== null) {
                    modifyImageCases.push(3);
                    const existingImageId = pareseRemoteImageUrlToDelete(trackImages[i].remoteUrl);

                    //const existingImageId = tokens[tokens.length - 1].split('.')[0]; //this may include folder to delete image asset
                    modifiableImages.push(trackImages[i].remoteUrl); // removing this will cause bug. 
                    modifySignRequestPromises.push(
                        recordService.getUploadApiSignature({
                            public_id: existingImageId,
                            invalidate: 'true'
                        })
                    );
                }
            }
            else {
                if (trackImages[i].remoteUrl !== null) {
                    imageIds.push(trackImages[i].remoteUrl);
                }
            }
        }

        Promise.all(modifySignRequestPromises)
            .then(
                (result) => {
                    result.map(
                        (res, i) => {
                            switch (modifyImageCases[i]) {
                                case 1:
                                    modifyImagePromises.push(
                                        cloudinaryService.uploadImage({ ...res.body }, modifiableImages[i])
                                    );
                                    break;
                                case 2:
                                    modifyImagePromises.push(
                                        cloudinaryService.uploadImage({ ...res.body }, modifiableImages[i])
                                    );
                                    break;
                                case 3:
                                    console.log('case 3');
                                    console.log(res.body.public_id);
                                    modifyImagePromises.push(
                                        cloudinaryService.destroyImage({ ...res.body })
                                    );
                                    break;
                                default:
                                    break;
                            }
                        }
                    );

                    return Promise.all(modifyImagePromises);
                }
            ).then(
                (result) => {
                    result.map(
                        (res) => {
                            console.log(res);
                            if (res.body.secure_url !== undefined) imageIds.push(res.body.secure_url);
                        }
                    );

                    console.log(imageIds);
                    const imageString = imageIds.join(',');
                    console.log(rowData[0])
                    console.log({ ...formState.values });
                    recordService.updateRecord({
                        ...formState.values,
                        images: imageString
                    }, rowData[0]
                    ).catch(err => {
                        if (err.response.body.errors === "Invalid token") setShouldRefreshToken(true);
                    });

                    setLoading(false);
                    closeDialog("update");
                }
            ).catch(err => {
                console.log(err.body);
                if (err.body &&
                    err.response.body.errors &&
                    err.response.body.errors === "Invalid token"
                ) setShouldRefreshToken(true);
            });
    };

    const hasError = field =>
        formState.touched[field] && formState.errors[field] ? true : false;

    useEffect(() => {
        const errors = validate(formState.values, schema);

        setFormState(formState => ({
            ...formState,
            isValid: errors ? false : true,
            errors: errors || {}
        }));
    }, [formState.values]);

    //load current images
    useEffect(() => {
        if (rowData[4] === null) return;
        else {
            let currentTrackImages = trackImages.slice();
            let imagePaths = rowData[4].split(',');
            for (let i = 0; i < imagePaths.length; i++) {
                currentTrackImages[i] = {
                    remoteUrl: imagePaths[i],
                    localUrl: '',
                    isModified: false
                }
            }
            setTrackImages(currentTrackImages);
        }
    }, []);

    return (
        <Dialog
            maxWidth="sm"
            fullWidth={true}
            onClose={onClose}
            open={open}
            disableBackdropClick
            disableEscapeKeyDown
        >
            <DialogTitle id="simple-dialog-title" >
                <div className={classes.wrapContentHeading}>
                    <Typography align="left" variant="body1" style={{ display: 'inline-block' }}>
                        <b> Update Mail Record </b>
                    </Typography>
                    <IconButton
                        className={classes.closeIcon}
                        onClick={onClose}>
                        <CloseIcon />
                    </IconButton>
                </div>
            </DialogTitle>
            <Divider />
            <div className={classes.content}>
                <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                >
                    <Grid item xs={12}>
                        <Grid item>
                            <TextField
                                error={hasError('from')}
                                helperText={
                                    hasError('from') ? formState.errors.from[0] : null
                                }
                                label="From"
                                name="from"
                                type="text"
                                fullWidth
                                onChange={handleChange}
                                value={formState.values.from || ''}
                                variant="outlined"
                            />
                            <TextField
                                error={hasError('subject')}
                                helperText={
                                    hasError('subject') ? formState.errors.subject[0] : null
                                }
                                label="Subject"
                                name="subject"
                                type="text"
                                fullWidth
                                onChange={handleChange}
                                value={formState.values.subject || ''}
                                variant="outlined"
                                className={classes.childContent}
                            />
                            <TextField
                                error={hasError('description')}
                                helperText={
                                    hasError('description') ? formState.errors.description[0] : null
                                }
                                label="Description"
                                name="description"
                                type="text"
                                fullWidth
                                onChange={handleChange}
                                value={formState.values.description || ''}
                                variant="outlined"
                                className={classes.childContent}
                            />
                            <div className={classes.imageContainer}>
                                {[...trackImages]
                                    .map((e, i) =>
                                        trackImages[i].localUrl === null ?
                                            <div key={i}>
                                                <input
                                                    accept="image/*"
                                                    className={classes.input}
                                                    id={`upload_${i}`}
                                                    multiple={false}
                                                    type="file"
                                                    onChange={(e) => handleImageUpload(e, i)}
                                                />
                                                <label htmlFor={`upload_${i}`}>
                                                    <AddIcon className={classes.addIcon} />
                                                </label>
                                            </div> :
                                            <div key={i}>
                                                <div className={classes.image} >

                                                    <img
                                                        src={
                                                            trackImages[i].isModified
                                                                ? URL.createObjectURL(trackImages[i].localUrl)
                                                                : trackImages[i].remoteUrl
                                                        }
                                                    />
                                                    <CancelIcon
                                                        color="error"
                                                        onClick={
                                                            (e) => resetImage(e, i)
                                                        }
                                                        className={classes.wrapImageIcon} />
                                                </div>
                                            </div>
                                    )}
                            </div>
                        </Grid>
                        <Grid
                            container
                            item
                            direction="row-reverse"
                            justify="flex-start"
                            alignItems="flex-start"
                        >
                            <Grid item>
                                <Button
                                    variant="contained"
                                    color="primary"
                                    disabled={!formState.isValid || loading}
                                    onClick={handleUpdate}
                                    className={classes.button}
                                >
                                    {loading &&
                                        <>
                                            <CircularProgress className={classes.circularProgress} size={20} thickness={5} />
                                            Saving...
                                        </>}
                                    {!loading && <span> Update </span>}
                                </Button>
                            </Grid>
                            <Grid item>
                                <Button
                                    variant="contained"
                                    color="secondary"
                                    disabled={!formState.isValid || deleting}
                                    onClick={handleDelete}
                                    className={classes.button}
                                >
                                    {deleting &&
                                        <>
                                            <CircularProgress className={classes.circularProgress} size={20} thickness={5} />
                                            Deleting...
                                        </>}
                                    {!deleting && <span> Delete </span>}
                                </Button>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </div>
        </Dialog>
    );
}

EditRecordDialog.propTypes = {
    onClose: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired
};