import React, { useState, useEffect } from 'react';

//libs
import { useHistory } from 'react-router-dom';
import validate from 'validate.js';

//UI libs
import { makeStyles } from '@material-ui/styles';
import {
  Button,
  TextField,
  Link,
  Typography,
  CssBaseline,
  CircularProgress
} from '@material-ui/core';

//component
import { CredentialForm } from '../../components';

//API services
import { authService } from '../../services';

//context
import { useUserContext } from '../../context/UsersContext';

const useStyles = makeStyles((theme) => ({
  form: {
    marginTop: theme.spacing(6),
  },
  textField: {
    marginTop: theme.spacing(2),
    [`& fieldset`]: {
      borderRadius: 0
    }
  },
  circularProgress: {
    marginLeft: 0,
    marginRight: theme.spacing(1),
  },
  heading: {
    fontWeight: 700,
    fontSize: 40,
    textTransform: 'uppercase'
  },
  button: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(2),
    borderRadius: 0,
    fontWeight: 700
  },
  feedback: {
    marginTop: theme.spacing(2),
  },
}));

const schema = {
  email: {
    presence: { allowEmpty: false, message: 'is required' },
    email: true,
    length: {
      maximum: 64
    }
  },
  password: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      maximum: 128
    }
  }
};

function LogIn() {
  const { setIsAuthenticated, setUser } = useUserContext();
  const history = useHistory();
  const classes = useStyles();
  const [isLoading, setIsLoading] = useState(false);
  const [formState, setFormState] = useState({
    isValid: false,
    values: {},
    touched: {},
    errors: {}
  });

  useEffect(() => {
    const errors = validate(formState.values, schema);
    setFormState(formState => ({
      ...formState,
      isValid: errors ? false : true,
      errors: errors || {}
    }));
  }, [formState.values]);

  const handleChange = event => {
    event.persist();
    setFormState(formState => ({
      ...formState,
      values: {
        ...formState.values,
        [event.target.name]: event.target.value
      },
      touched: {
        ...formState.touched,
        [event.target.name]: true
      }
    }));
  };

  const handleSignIn = event => {
    event.preventDefault();
    setIsLoading(true);
    console.log(formState.values.email);
    console.log(formState.values.password);
    authService.login({ ...formState.values })
      .then(res => {
        console.log(res.body);
        localStorage.setItem('SESSION_TOKEN', res.body.refreshToken);
        const { refreshToken, token, ...rest } = res.body;
        setUser({ ...rest });
        setIsAuthenticated(true); // set isAuthenticated to true which will help to redirct user to private route
        setIsLoading(false);
        history.push('/records');
      }).catch(err => {
        setIsLoading(false);
        console.log("failed to login...");
      });
  }

  const hasError = field =>
    formState.touched[field] && formState.errors[field] ? true : false;

  return (
    <React.Fragment>
      <CssBaseline />
      <CredentialForm>
        <>
          <Typography
            className={classes.heading}
            variant="h3"
            gutterBottom
            marked="center"
            align="center">
            Sign In
          </Typography>
        </>
        <TextField
          className={classes.textField}
          disabled={isLoading}
          error={hasError('email')}
          fullWidth
          helperText={
            hasError('email') ? formState.errors.email[0] : null
          }
          label='Email'
          name="email"
          onChange={handleChange}
          type="text"
          value={formState.values.email || ''}
          variant="outlined"
          autoFocus
          margin='normal'
        />
        <TextField
          className={classes.textField}
          disabled={isLoading}
          error={hasError('password')}
          fullWidth
          helperText={
            hasError('password') ? formState.errors.password[0] : null
          }
          label='Password'
          name="password"
          onChange={handleChange}
          type="password"
          value={formState.values.password || ''}
          variant="outlined"
          autoFocus
          margin='normal'
        />
        <Button
          className={classes.button}
          color="primary"
          disabled={!formState.isValid || isLoading}
          fullWidth
          onClick={handleSignIn}
          size="large"
          type="submit"
          variant="contained"
        >
          {isLoading &&
            <>
              <CircularProgress className={classes.circularProgress} size={20} thickness={5} />
            </>}
           Sign In
        </Button>
        <Typography align="center">
          <Link underline="always" href="#">
            Forgot password?
          </Link>
        </Typography>
      </CredentialForm>
    </React.Fragment>
  );
}

export default LogIn;