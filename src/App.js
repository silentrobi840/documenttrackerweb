import React, { useState, useEffect } from "react";

//libs
import {
  BrowserRouter as Router
} from "react-router-dom";

import Routes from './Routes';

//context
import { UserContext } from "./context/UsersContext";

//API services
import { authService } from './services';

export default function App() {
  const [user, setUser] = useState({});
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const [isAuthenticating, setIsAuthenticating] = useState(true);
  const [shouldRefreshToken, setShouldRefreshToken] = useState(false);

  const loadSession = () => {
    console.log('im refresh')
    authService.getCurrentUser()
      .then(
        (res) => {
          const { token, ...rest } = res.body;
          setUser({ ...rest });
          setIsAuthenticated(true)
        }
      )
      .catch(
        err => {
          setIsAuthenticated(false);
          console.log("Session Expired!");
          setUser({});
        });
    setIsAuthenticating(false);
  }

  useEffect(() => {
    loadSession();
  }, []);

  return (
    !isAuthenticating &&
    <UserContext.Provider value={{ isAuthenticated, setIsAuthenticated, shouldRefreshToken, setShouldRefreshToken, user, setUser }}>
      <Router>
        <Routes />
      </Router>
    </UserContext.Provider>
  );
}

