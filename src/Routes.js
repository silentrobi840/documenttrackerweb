import React from 'react';

//libs
import { Switch, Redirect } from 'react-router-dom';
import { PublicRouter, PrivateRouter } from './components/RouterWithLayout';

//layouts
import { Main as MainLayout } from './layouts';

// pages
import { LogIn, Records } from "./pages";

const Routes = () => {
    return (
        <Switch>
            <PublicRouter exact path="/login" component={LogIn} />
            <PublicRouter exact path="/not-found" component={Error} />
            <PrivateRouter exact path="/records" layout={MainLayout} component={Records} />
            <Redirect from="/" to="/login" />
            <Redirect to="/not-found" />
        </Switch>
    );
};

export default Routes;