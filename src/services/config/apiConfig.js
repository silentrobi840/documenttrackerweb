class ApiConfig {
    static token;
    static refreshToken;
    static expiredIn;
}

export default ApiConfig;
