import request from 'superagent';
import authHeader from './authHeader';

const BASE_URL = "http://localhost:3600";

class RecordService {

    createRecord(record) {
        return request.post(BASE_URL + '/record')
            .set(authHeader())
            .send(record);
    }

    getRecords() {
        return request.get(BASE_URL + '/record')
            .set(authHeader());
    }

    getRecord(id) {
        return request.get(BASE_URL + `/record/${id}`)
            .set(authHeader());
    }

    getUploadApiSignature(attrs) {
        return request.post(BASE_URL + '/upload-api-sign-request')
            .set(authHeader())
            .send(attrs);
    }

    updateRecord(attrs, id) {
        return request.patch(BASE_URL + `/record/${id}`)
            .set(authHeader())
            .send(attrs);
    }

    deleteRecord(id) {
        return request.delete(BASE_URL + `/record/${id}`)
            .set(authHeader());
    }

}

const recordService = new RecordService();
export default recordService;