import request from 'superagent';
import authHeader from './authHeader';

const BASE_URL = "http://localhost:3600";

class UserService {

    createUser(user) {
        return request.post(BASE_URL + '/user')
            .set(authHeader())
            .field(user);
    }

    getUsers() {
        return request.get(BASE_URL + '/user')
            .set(authHeader());
    }

    getUser(id) {
        return request.get(BASE_URL + `/user/${id}`)
            .set(authHeader());
    }

    getCurrentUserId() {
        return request.get(BASE_URL + '/currentUserUid')
            .set(authHeader());
    }

    updateUser(attrs, id) {
        return request.patch(BASE_URL + `/user/${id}`)
            .set(authHeader())
            .field(attrs);
    }

    deleteUser(id) {
        return request.delete(BASE_URL + `/user/${id}`)
            .set(authHeader());
    }

}

const userService = new UserService();
export default userService;