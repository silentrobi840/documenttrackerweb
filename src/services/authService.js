import request from 'superagent';
import ApiConfig from './config/apiConfig';
import authHeader from './authHeader';

const BASE_URL = "http://localhost:3600/auth";

class AuthService {
    login(loginAttrs) {
        console.log(loginAttrs);
        return request.post(BASE_URL + '/login')
            .set('credentials', 'same-origin')
            .send(loginAttrs)
            .withCredentials()
            .then(res => {
                ApiConfig.token = res.body.token;
                ApiConfig.expiredIn = res.body.expiresIn;
                return res;
            });
    }

    // current logout mechanism is not intellegent
    logout() {
        ApiConfig.token = null;
        // make api call to put refresh token in block list in server: future modification
        localStorage.removeItem('SESSION_TOKEN');
    }

    register(registerAttrs) {
        return request.post(BASE_URL + '/register')
            .set(authHeader())
            .field(registerAttrs);
    }

    getCurrentUser() {
        console.log(localStorage.getItem('SESSION_TOKEN'))
        return request.post(BASE_URL + '/refresh')
            .send({refreshToken: localStorage.getItem('SESSION_TOKEN')})
            .then((res) => {
                ApiConfig.token = res.body.token;
                ApiConfig.expiredIn = res.body.expiredIn;
                return res;
            });
    }
}

const authService = new AuthService();
export default authService;