import request from 'superagent';
import cloudinaryConfig from './config/cloudinaryConfig';

const BASE_URL = `https://api.cloudinary.com/v1_1/${cloudinaryConfig.cloudName}/image`;

class CloudinaryService {

    uploadImage(attrs, imageFile) {
        return request.post(BASE_URL + '/upload')
            .field(attrs)
            .field('file', imageFile);
    }

    destroyImage(attrs) {
        return request.post(BASE_URL + '/destroy')
            .field(attrs);
    }

}

const cloudinaryService = new CloudinaryService();
export default cloudinaryService;