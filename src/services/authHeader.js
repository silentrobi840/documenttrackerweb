import ApiConfig from './config/apiConfig';

export default function authHeader() {
    if (ApiConfig.token) {
        return {
            'Authorization': `Bearer ${ApiConfig.token}`
        };
    }
    else {
        return {};
    }
}